import { Route, Routes } from "react-router-dom";
import { Authorization } from "./pages/Authorization";
import { Main } from "./pages/Main";
import { CreateOrganization } from "./pages/CreateOrganization";
import { useSelector } from "react-redux";
import { SuggestionForm } from "./components/SuggestionForm";
import { UserForm } from "./components/UserForm";
import { EmailNotification } from "./components/EmailNotification";

export const App = () => {
  const isSuggestionFormOpen = useSelector(
    (state) => state.modalReducer.isSuggestionFormOpen
  );
  const isUserFormOpen = useSelector(
    (state) => state.modalReducer.isUserFormOpen
  );
  const isEmailNotificationOpen = useSelector(
    (state) => state.modalReducer.isEmailNotificationOpen
  );

  return (
    <div>
      <div
        className={`${
          (isSuggestionFormOpen || isUserFormOpen) &&
        "bg-black backdrop-contrast-50"}
          ${isEmailNotificationOpen && "blur-md"}
        `}
      >
        <Routes>
          <Route path="/" element={<Authorization />} />
          <Route path="/create-organization" element={<CreateOrganization />} />
          <Route path="/user" element={<Main />} />
        </Routes>
      </div>
      {isSuggestionFormOpen && <SuggestionForm />}
      {isUserFormOpen && <UserForm />}
      {isEmailNotificationOpen && <EmailNotification/>}
    </div>
  );
};
