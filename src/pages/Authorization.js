import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { BACKEND_URL, HTTP_STATUS } from "../components/Constants";
import { useState } from "react";

export const Authorization = () => {
  const {
    register,
    formState: { errors, isValid },
    handleSubmit,
    reset,
  } = useForm({ mode: "onBlur", shouldUnregister: true });

  const [httpStatus, setHttpStatus] = useState(null);

  const onSubmit = (data) => {
    console.log('test')
    axios
      .post(BACKEND_URL + "users/token", data)
      .then((response) => {
        const token = response.data.access_token;
        localStorage.setItem("token", token);
        
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        
        axios.get(BACKEND_URL + "users/me")
          .then((response) => {
            const role = response.data.level;
            console.log(response)
            navigate("/user");
            localStorage.setItem("role", role);
          });
      })
      .catch((error) => {
        if (error.response && error.response.status === 401) {
          setHttpStatus(HTTP_STATUS.UNAUTHORIZED);
        } else {
          setHttpStatus(HTTP_STATUS.OTHER);
        }
      });
  };

  const navigate = useNavigate();

  return (
    <div className="h-screen w-screen flex items-center justify-center">
      <div className="flex justify-between flex-col bg-stone-400 h-3/5 w-1/4 p-4">
        <h1 className="font-bold font-ultraExtendedBold text-3xl text-red flex justify-center">
          Yet Another Course
        </h1>
        <div>
        <h1 className="text-2xl mb-10 font-bold font-compactMedium tracking-wide">
          Войти в профиль
        </h1>
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="flex flex-col justify-between "
        >
          <label
            className=" text-field_label mb-2 color-field_label"
            for="email"
          >
            E-mail
          </label>
          <input
            {...register("email", {
              required: true,
              pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            })}
            className="border-2 border-field_border placeholder-placeholder bg-grey rounded-lg py-3 px-2 mb-2"
            type="text"
            placeholder="Email"
            name="email"
            id="email"
          />
          <label
            className="text-field_label mb-2 color-field_label mt-4"
            for="password"
          >
            Пароль
          </label>
          <input
            {...register("password", {
              required: true,
            })}
            className="border-2 border-field_border placeholder-placeholder bg-grey rounded-lg py-3 px-2 mb-2"
            type="password"
            placeholder="Пароль"
            id="password"
            name="password"
          />
          {httpStatus === HTTP_STATUS.UNAUTHORIZED && (
            <p className="text-red">Неверный логин или пароль</p>
          )}
          {httpStatus === HTTP_STATUS.OTHER && (
            <p className="text-red">Что-то пошло не так, попробуйте еще раз</p>
          )}
          <div className="flex justify-between flex-col">
            <button
              className="bg-red p-2 my-2 rounded-lg py-3 mt-8"
              type="submit"
            >
              <p className="text-white"> Войти</p>
            </button>
            <button
              className="bg-grey text-black p-2 my-2 rounded-lg py-3 mt-14"
              type="button"
              onClick={() => {
                navigate("/create-organization");
              }}
            >
              Создать организацию
            </button>
          </div>
          </form>
          </div>
      </div>
    </div>
  );
};
