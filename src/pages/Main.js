import { ButtonContainer } from "../components/ButtonContainer";
import { Suggestion } from "../components/Suggestion";
import { Header } from "../components/Header";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { BACKEND_URL, ROLES, SECTIONS } from "../components/Constants";
import axios from "axios";
import { Application } from "../components/Application";
import {
  setManagerRequests,
  setSuggestions,
  setUserRequests,
  setUsers,
} from "../store/slices/DataSlice";
import { User } from "../components/User";

export const Main = () => {
  const isSuggestionFormOpen = useSelector(
    (state) => state.modalReducer.isSuggestionFormOpen
  );
  const isUserFormOpen = useSelector(
    (state) => state.modalReducer.isUserFormOpen
  );
  const isEmailNotificationOpen = useSelector(
    (state) => state.modalReducer.isEmailNotificationOpen
  )

  const counter = useSelector((state) => state.updateReducer.updateSwitch);

  const suggestions = useSelector((state) => state.dataReducer.suggestions);
  const userRequests = useSelector((state) => state.dataReducer.userRequests);
  const managerRequests = useSelector(
    (state) => state.dataReducer.managerRequests
  );
  const users = useSelector((state) => state.dataReducer.users);

  const dispatch = useDispatch();

  const [level, setLevel] = useState(localStorage.getItem("role"));

  useEffect(() => {
    axios.get(BACKEND_URL + "users/suggestions").then((response) => {
      dispatch(setSuggestions(response.data));
    });
    const token = localStorage.getItem("token");
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    axios.get(BACKEND_URL + "requests/get_requests").then((response) => {
      dispatch(setUserRequests(response.data.user_requests));
      console.log(response.data);
    });
    if (level === ROLES.MANAGER) {
      axios.get(BACKEND_URL + "managers/get_requests").then((response) => {
        dispatch(setManagerRequests(response.data.requests));
        console.log(response.data);
      });
    }
    if (level === ROLES.ADMIN || level === ROLES.OWNER) {
      axios.get(BACKEND_URL + "admins/get_users").then((response) => {
        dispatch(setUsers(response.data.users));
        console.log(response.data);
      });
    }
  }, [counter]);

  const selectedSection = useSelector(
    (state) => state.selectedSectionReducer.selectedSection
  );

  return (
    <div className="flex flex-col justify-center h-screen w-screen">
      <Header role={localStorage.getItem("role")} />

      <div
        className={`flex flex-col justify-start items-center bg-grey h-[90%] w-full  ${
          (isSuggestionFormOpen || isUserFormOpen || isEmailNotificationOpen) &&
          "opacity-30"
        }`}
      >
        <div className="flex w-[50%] h-full flex-col">
          <ButtonContainer />
          <div className="overflow-y-scroll pr-5">
            {(selectedSection === SECTIONS.SUGGESTIONS ||
              selectedSection === SECTIONS.MY_SUGGESTIONS) &&
              suggestions.map((suggestion) => {
                return (
                  <Suggestion
                    key={suggestion.id}
                    id={suggestion.id}
                    title={suggestion.name}
                    link={suggestion.link}
                    price={suggestion.price}
                    start_date={
                      new Date(suggestion.start_date)
                        .toISOString()
                        .split("T")[0]
                    }
                    end_date={
                      new Date(suggestion.end_date).toISOString().split("T")[0]
                    }
                    quarter={suggestion.quarter_of_training}
                  />
                );
              })}
            {(selectedSection === SECTIONS.APPROVED ||
              selectedSection === SECTIONS.IN_PROGRESS ||
              selectedSection === SECTIONS.REJECTED) &&
              userRequests
                .filter((request) => request.status === selectedSection)
                .map((request) => {
                  return (
                    <Suggestion
                      key={request.id}
                      id={request.id}
                      reason={request.reason}
                      title={request.event.name}
                      link={request.event.link}
                      price={request.event.price}
                      start_date={
                        new Date(request.event.start_date)
                          .toISOString()
                          .split("T")[0]
                      }
                      end_date={
                        new Date(request.event.end_date)
                          .toISOString()
                          .split("T")[0]
                      }
                      quarter={request.event.quarter_of_training}
                    />
                  );
                })}
            {selectedSection === SECTIONS.STAFF_APPLICATIONS &&
              managerRequests.map((request) => {
                return (
                  <Application
                    key={request.id}
                    id={request.id}
                    reason={request.reason}
                    status={request.status}
                    full_name={request.user.full_name}
                    title={request.event.name}
                    link={request.event.link}
                    price={request.event.price}
                    start_date={
                      new Date(request.event.start_date)
                        .toISOString()
                        .split("T")[0]
                    }
                    end_date={
                      new Date(request.event.end_date)
                        .toISOString()
                        .split("T")[0]
                    }
                    quarter_of_training={request.event.quarter_of_training}
                  />
                );
              })}
            {selectedSection === SECTIONS.USERS_MANAGEMENT &&
              users.map((user) => {
                return (
                  <User
                    id={user.id}
                    key={user.id}
                    full_name={user.full_name}
                    department={user.department}
                    email={user.email}
                    level={user.level}
                  />
                );
              })}
          </div>
        </div>
      </div>
    </div>
  );
};
