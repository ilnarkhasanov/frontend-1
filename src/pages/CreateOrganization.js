import axios from "axios";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { BACKEND_URL } from "../components/Constants";
import { useState } from "react";
import { HTTP_STATUS } from "../components/Constants";
import { useDispatch } from "react-redux";
import { openEmailNotification } from "../store/slices/ModalSlice";

export const CreateOrganization = () => {
  const {
    register,
    formState: { errors, isValid },
    handleSubmit,
    reset,
  } = useForm({ mode: "onBlur", shouldUnregister: true });

  const [httpStatus, setHttpStatus] = useState(null);

  const onSubmit = (data) => {
    axios
      .post(BACKEND_URL + "organization/register", data)
      .then(() => {
        dispatch(openEmailNotification())

      })
      .catch((error) => {
        if (error.response && error.response.status === HTTP_STATUS.BAD_REQUEST) {
          setHttpStatus(HTTP_STATUS.BAD_REQUEST);
        } else {
          setHttpStatus(HTTP_STATUS.OTHER);
        }
      });
  };

  const dispatch = useDispatch()
  const navigate = useNavigate();

  return (
    <div className="h-screen w-screen flex items-center justify-center">
      <div className="flex justify-between flex-col bg-stone-400 h-3/5 w-1/4 p-4">
        <h1 className="font-bold font-ultraExtendedBold text-3xl text-red flex justify-center">
          Yet Another Course
        </h1>
        <div>
          <h1 className="text-2xl mb-10 font-bold font-compactMedium tracking-wide">
            Создать организацию
          </h1>
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col justify-between"
          >
            <label className="mb-2 color-field_label " for="name">
              Название организации
            </label>
            <input
              {...register("name", {
                required: true,
              })}
              className="border-2 border-field_border placeholder-placeholder bg-grey rounded-lg py-3 px-2 mb-2"
              type="text"
              name="name"
              id="name"
            />
            <label className="mb-2 color-field_label mt-4" for="owner_email">
              E-mail владельца
            </label>
            <input
              {...register("owner_email", {
                required: true,
              })}
              className="border-2 border-field_border placeholder-placeholder bg-grey rounded-lg py-3 px-2 mb-2"
              type="text"
              name="owner_email"
              id="owner_email"
            />
            {httpStatus === HTTP_STATUS.BAD_REQUEST && (
              <p className="text-red">Почта уже зарегистрирована</p>
            )}
            <div className="flex justify-between flex-col">
              <button
                className="bg-red p-2 my-2 rounded-lg py-3 mt-8"
                type="submit"
              >
                <p className="text-white">Зарегистрировать</p>
              </button>
              <button
                className="bg-grey text-black p-2 my-2 rounded-lg py-3 mt-14"
                type="button"
                onClick={() => {
                  navigate("/")
                }}
              >
                Назад
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
