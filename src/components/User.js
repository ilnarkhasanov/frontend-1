import { useDispatch } from "react-redux";
import { openUserForm } from "../store/slices/ModalSlice";
import { BACKEND_URL, ROLES } from "./Constants";
import axios from "axios";
import { updateSwitchCounter } from "../store/slices/UpdateSlice";
import { setSelectedUser } from "../store/slices/SelectedUserSlice";

export const User = (props) => {
  const dispatch = useDispatch();
  const deleteUser = () => {
    axios.delete(BACKEND_URL + `admins/delete/${props.id}`).then(() => {
      console.log("User deleted");
      dispatch(updateSwitchCounter());
    });
  };

  const updateUser = () => {
    const formData = {
      full_name: props.full_name,
      email: props.email,
      department: props.department,
      level: props.level,
      id: props.id,
    };
    dispatch(setSelectedUser(formData));
    dispatch(openUserForm());
  };

  return (
    <div className="flex flex-row justify-between w-full h-fit bg-white rounded-xl mb-6 py-6 px-5">
      <div className="flex flex-col justify-between">
        <h2 className="text-3xl">{props.full_name}</h2>
        <p className="text-xl font-textRegular mt-2">
          {props.department} • {props.level}
        </p>
        <p className="text-xl font-textRegular italic">{props.email}</p>
      </div>

      {props.level !== ROLES.OWNER && (
        <div className="flex flex-col justify-end items-end">
          <div className="flex flex-row">
            <button
              onClick={() => {
                updateUser();
              }}
              className="bg-red p-2 my-2 mr-3 rounded px-5 text-white"
            >
              Изменить
            </button>

            <button
              onClick={() => {
                deleteUser();
              }}
              className="bg-grey p-2 my-2 mr-3 rounded px-5 text-black"
            >
              Удалить
            </button>
          </div>
        </div>
      )}
    </div>
  );
};
