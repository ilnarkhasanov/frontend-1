import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import CloseCross from "../assets/CloseCross.png";
import { closeUserForm } from "../store/slices/ModalSlice";
import { BACKEND_URL, SECTIONS } from "./Constants";
import { setSelectedSection } from "../store/slices/SelectedSectionSlice";
import axios from "axios";
import { updateSwitchCounter } from "../store/slices/UpdateSlice";

export const UserForm = () => {
  const {
    register,
    formState: { errors, isValid },
    handleSubmit,
    reset,
  } = useForm({ mode: "onBlur", shouldUnregister: true });

  const onSubmit = (data) => {
    if (selectedSection === SECTIONS.ADD_USER) {
      axios.post(BACKEND_URL + "admins/create_user", data).then((response) => {
        console.log(response);
        dispatch(setSelectedSection(SECTIONS.USERS_MANAGEMENT));
        dispatch(closeUserForm());
        dispatch(updateSwitchCounter());
      });
    }
    else {
      console.log(data)
      axios.put(BACKEND_URL + `admins/update/${selectedUser.id}`, data).then((response) => {
        console.log(response);
        dispatch(setSelectedSection(SECTIONS.USERS_MANAGEMENT));
        dispatch(closeUserForm());
        dispatch(updateSwitchCounter());
      }
      );
    }
  };

  const selectedUser = useSelector(
    (state) => state.selectedUserReducer.selectedUser
  );

  const dispatch = useDispatch();
  const selectedSection = useSelector(
    (state) => state.selectedSectionReducer.selectedSection
  );

  const closeWindowLogic = () => {
    dispatch(setSelectedSection(SECTIONS.USERS_MANAGEMENT));
    dispatch(closeUserForm());
  };

  return (
    <div
      className="absolute top-[25%] left-1/2 transform -translate-x-1/2 -translate-y-[20%] w-1/3 h-fit
    rounded-3xl bg-white flex flex-col justify-around  px-5 py-6"
    >
      <div className="relative flex justify-end items-end w-full bg-white">
        <img
          className="absolute h-8 w-8 -right-14 -top-5"
          onClick={() => {
            closeWindowLogic();
          }}
          src={CloseCross}
          alt="Close Cross"
        ></img>
      </div>
      <h1 className="text-2xl mb-3 font-compactMedium">
        {selectedSection === SECTIONS.USERS_MANAGEMENT
          ? "Изменить данные пользователя"
          : "Добавить нового пользователя"}
      </h1>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="w-full flex flex-col justify-between "
      >
        <label className="text-sm mb-2 mt-4 text-field_label" for="full_name">
          ФИО
        </label>
        <input
          {...register("full_name", {
            required: true,
          })}
          className={`border-2 border-field_border placeholder-placeholderbg-grey rounded-lg py-3 px-2 mb-2`}
          type="text"
          name="full_name"
          id="full_name"
          defaultValue={
            selectedSection === SECTIONS.USERS_MANAGEMENT
              ? selectedUser.full_name
              : ""
          }
        />
        <label className="text-sm mb-2 mt-4 text-field_label" for="department">
          Департамент
        </label>
        <input
          {...register("department", {
            required: true,
          })}
          className={`border-2 border-field_border placeholder-placeholderbg-grey rounded-lg py-3 px-2 mb-2`}
          type="text"
          name="department"
          id="department"
          defaultValue={
            selectedSection === SECTIONS.USERS_MANAGEMENT
              ? selectedUser.department
              : ""
          }
        />
        <label className="text-sm mb-2 mt-4 text-field_label" for="level">
          Роль
        </label>
        <select
          {...register("level", {
            required: true,
          })}
          className={`border-2 border-field_border placeholder-placeholder ${
            selectedSection === SECTIONS.SUGGESTIONS
              ? "bg-field_border"
              : "bg-grey"
          } rounded-lg py-3 px-2 mb-2`}
          type="text"
          id="level"
          name="level"
          defaultValue={
            selectedSection === SECTIONS.USERS_MANAGEMENT
              ? selectedUser.level
              : ""
          }
        >
          <option value="User">Пользователь</option>
          <option value="Administrator">Администратор</option>
          <option value="Manager">Менеджер</option>
        </select>

        <label className="text-sm mb-2 mt-4 text-field_label" for="email">
          Email
        </label>
        <input
          {...register("email", {
            required: true,
          })}
          className={`border-2 border-field_border placeholder-placeholderbg-grey rounded-lg py-3 px-2 mb-2`}
          type="text"
          name="email"
          id="email"
          defaultValue={
            selectedSection === SECTIONS.USERS_MANAGEMENT
              ? selectedUser.email
              : ""
          }
        />

        <button
          className="flex self-end justify-center bg-red px-6 py-3 my-2 rounded-lg mt-8"
          type="submit"
        >
          <p className="text-white">
            {selectedSection === SECTIONS.USERS_MANAGEMENT
              ? "Изменить"
              : "Добавить"}
          </p>
        </button>
      </form>
    </div>
  );
  };
  
