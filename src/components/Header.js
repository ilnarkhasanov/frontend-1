import UserIcon from "../assets/UserIcon.png";
import { useSelector, useDispatch } from "react-redux";
import { openSuggestionForm } from "../store/slices/ModalSlice";
import { setSelectedSection } from "../store/slices/SelectedSectionSlice";
import { ROLES, SECTIONS } from "./Constants";
import { Popover } from "@headlessui/react";
import { useNavigate } from "react-router-dom";

export const Header = (props) => {
  const isSuggestionFormOpen = useSelector(
    (state) => state.modalReducer.isSuggestionFormOpen
  );
  const isUserFormOpen = useSelector(
    (state) => state.modalReducer.isUserFormOpen
  );


  const exit = () => {
    console.log("test")
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    dispatch(setSelectedSection(SECTIONS.SUGGESTIONS))
    navigate("/");
  }

  const navigate = useNavigate();
  const dispatch = useDispatch();

  return (
    <div
      className={`w-full h-[10%] bg-white ${
        (isSuggestionFormOpen || isUserFormOpen) &&
        "opacity-50"
      }`}
    >
      <header className="flex h-full full mx-14 justify-between items-center">
        <div className="flex flex-row items-center">
          <h1 className="block font-bold font-ultraExtendedBold text-3xl text-red">
            Yet Another Course
          </h1>
          {props.role === ROLES.MANAGER && (
            <button
              className="px-5 py-3 font-semibold text-lg rounded-lg ml-4 bg-grey"
              onClick={() => {
                dispatch(setSelectedSection(SECTIONS.MY_SUGGESTIONS));
              }}
            >
              Мои предложения
            </button>
          )}
        </div>
        <div className="w-[10%] flex justify-center items-center">
          <h1 className="block  text-xl font-compactRegular font-lg">
            {props.role}
          </h1>
          <Popover className="relative w-max">
            <Popover.Button>
              <img
                className="flex h-[30%] w-[30%] rounded-full ml-14"
                src={UserIcon}
                alt={"User Icon"}
              ></img>
            </Popover.Button>
            <Popover.Panel
              onClick={() => exit()}
              className="flex px-12 py-3 absolute z-10 bg-red text-white rounded-lg mt-8"
            >
              <button>Выйти</button>
            </Popover.Panel>
          </Popover>
        </div>
      </header>
    </div>
  );
};
