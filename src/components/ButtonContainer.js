import { useSelector } from "react-redux";

import { ROLES, SECTIONS } from "./Constants";

import { BackButton } from "./buttons/BackButton";

import { SectionButton } from "./buttons/SectionButton";
import { useState } from "react";

export const ButtonContainer = () => {
  const selectedSection = useSelector(
    (state) => state.selectedSectionReducer.selectedSection
  );

  const appoved = useSelector((state) => state.dataReducer.appoved);
  const inProgress = useSelector((state) => state.dataReducer.inProgress);
  const rejected = useSelector((state) => state.dataReducer.rejected);

  const [level, setLevel] = useState(localStorage.getItem("role"));

  return (
    <div className="h-fit w-5/6 flex justify-start items-center mt-8 mb-6">
      {(selectedSection === SECTIONS.SUGGESTIONS ||
        selectedSection === SECTIONS.NEW_APPLICATION) && (
        <>
          <SectionButton text="Предложения" section={SECTIONS.SUGGESTIONS} />
          {}
          <SectionButton
            text="Новая заявка"
            section={SECTIONS.NEW_APPLICATION}
            openSuggestionForm={true}
          />

          <SectionButton text="Мои заявки" section={SECTIONS.APPROVED} />
          {(level === ROLES.ADMIN || level === ROLES.OWNER) && (
            <SectionButton
              text="Управление пользователями"
              section={SECTIONS.USERS_MANAGEMENT}
            />
          )}
          {level === ROLES.MANAGER && (
            <SectionButton
              text="Заявки сотрудников"
              section={SECTIONS.STAFF_APPLICATIONS}
            />
          )}
        </>
      )}
      {(selectedSection === SECTIONS.APPROVED ||
        selectedSection === SECTIONS.IN_PROGRESS ||
        selectedSection === SECTIONS.REJECTED) && (
        <>
          <BackButton />
          <SectionButton
            text="Принято"
            section={SECTIONS.APPROVED}
            number={appoved}
          />
          <SectionButton
            text="В ожидании"
            section={SECTIONS.IN_PROGRESS}
            number={inProgress}
          />
          <SectionButton
            text="Отклонено"
            section={SECTIONS.REJECTED}
            number={rejected}
          />
        </>
      )}
      {selectedSection === SECTIONS.STAFF_APPLICATIONS && (
          <BackButton />
      )}
      {(selectedSection === SECTIONS.USERS_MANAGEMENT ||
        selectedSection === SECTIONS.ADD_USER) && (
        <>
          <BackButton />
          <SectionButton
            text="Добавить пользователя"
            section={SECTIONS.ADD_USER}
            openUserForm={true}
          />
        </>
      )}
      {selectedSection === SECTIONS.MY_SUGGESTIONS && (
        <>
          <BackButton />
          <SectionButton
            text="Добавить предложение"
            section={SECTIONS.ADD_SUGGESTION}
            openSuggestionForm={true}
          />
        </>
      )}
    </div>
  );
};
