import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import CloseCross from "../assets/CloseCross.png";
import { closeSuggestionForm } from "../store/slices/ModalSlice";
import { BACKEND_URL, SECTIONS } from "./Constants";
import { setSelectedSection } from "../store/slices/SelectedSectionSlice";
import axios from "axios";
import { Error } from "./Error";

import { updateSwitchCounter } from "../store/slices/UpdateSlice";

export const SuggestionForm = () => {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm({ mode: "onBlur", shouldUnregister: true });

  const selectedSuggestion = useSelector(
    (state) => state.selectedSuggestionReducer.suggestionData
  );

  const onSubmit = (data) => {
    const event = {
      name: data.name,
      link: data.link,
      price: Number(data.price),
      start_date: data.start_date,
      end_date: data.end_date,
      quarter_of_training: data.quarter_of_training,
    };

    const token = localStorage.getItem("token");
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;

    if (selectedSection === SECTIONS.ADD_SUGGESTION) {
      axios.post(BACKEND_URL + "managers/suggestion", event).then(() => {
        dispatch(setSelectedSection(SECTIONS.MY_SUGGESTIONS));
        dispatch(updateSwitchCounter());
        dispatch(closeSuggestionForm());
      });
    } else if (selectedSection === SECTIONS.MY_SUGGESTIONS) {
      const put_event = {
        event_id: selectedSuggestion.id,
        name: data.name,
        link: data.link,
        price: Number(data.price),
        start_date: data.start_date,
        end_date: data.end_date,
        quarter_of_training: data.quarter_of_training,
      };
      console.log(put_event);
      axios.put(BACKEND_URL + `managers/suggestion/`, put_event).then(() => {
        dispatch(updateSwitchCounter());
        dispatch(closeSuggestionForm());
      });
    } else if (selectedSection === SECTIONS.IN_PROGRESS || selectedSection === SECTIONS.REJECTED) {
      const result = {
        reason: data.reason,
        event: event,
      };
      console.log(result);
      axios
        .put(
          BACKEND_URL + `requests/update_request/${selectedSuggestion.id}`,
          result
        )
        .then(() => {
          dispatch(updateSwitchCounter());
          dispatch(closeSuggestionForm());
        });
    } else {
      const result = {
        reason: data.reason,
        event: event,
      };
      axios.post(BACKEND_URL + "requests/new_request", result).then(() => {
        if (selectedSection === SECTIONS.NEW_APPLICATION) {
          dispatch(setSelectedSection(SECTIONS.SUGGESTIONS));
        }
        dispatch(updateSwitchCounter());
        dispatch(closeSuggestionForm());
      });
    }
  };

  const dispatch = useDispatch();
  const selectedSection = useSelector(
    (state) => state.selectedSectionReducer.selectedSection
  );
  const suggestionData = useSelector(
    (state) => state.selectedSuggestionReducer.suggestionData
  );

  const closeWindowLogic = () => {
    if (selectedSection === SECTIONS.NEW_APPLICATION) {
      dispatch(setSelectedSection(SECTIONS.SUGGESTIONS));
    }
    if (selectedSection === SECTIONS.ADD_SUGGESTION) {
      dispatch(setSelectedSection(SECTIONS.MY_SUGGESTIONS));
    }
    dispatch(closeSuggestionForm());
  };

  return (
    <div
      className="absolute top-[25%] left-1/2 transform -translate-x-1/2 -translate-y-[20%] w-1/3 h-fit
    rounded-3xl bg-white flex flex-col justify-around  px-5 py-6"
    >
      <div className="relative flex justify-end items-end w-full bg-white">
        <img
          className="absolute h-8 w-8 -right-14 -top-5"
          onClick={() => {
            closeWindowLogic();
          }}
          src={CloseCross}
          alt="Close Cross"
        ></img>
      </div>
      <h1 className="text-2xl mb-3 font-compactMedium">
        {selectedSection === SECTIONS.MY_SUGGESTIONS && "Изменение предложения"}
        {(selectedSection === SECTIONS.SUGGESTIONS ||
          selectedSection === SECTIONS.NEW_APPLICATION) &&
          "Заявка на предложение"}
        {selectedSection === SECTIONS.IN_PROGRESS ||
          (selectedSection === SECTIONS.REJECTED && "Изменение заявки")}
      </h1>
      <p className="block mb-6 font-compactRegular">
        {selectedSection === SECTIONS.MY_SUGGESTIONS && "Изменение предложения"}
        {(selectedSection === SECTIONS.SUGGESTIONS ||
          selectedSection === SECTIONS.NEW_APPLICATION) &&
          "Оставьте данные для получения одобрения"}
        {selectedSection === SECTIONS.IN_PROGRESS ||
          (selectedSection === SECTIONS.REJECTED && "Изменение заявки")}
      </p>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="w-full flex flex-col justify-between "
      >
        <label className="text-sm mb-2 text-field_label" for="name">
          Название курса
        </label>
        <input
          {...register("name", {
            required: true,
          })}
          className={`border-2 border-field_border placeholder-placeholder ${
            selectedSection === SECTIONS.SUGGESTIONS
              ? "bg-field_border"
              : "bg-grey"
          } rounded-lg py-3 px-2 mb-2`}
          type="text"
          name="name"
          id="name"
          value={
            selectedSection === SECTIONS.SUGGESTIONS
              ? suggestionData.title
              : undefined
          }
          defaultValue={
            selectedSection === SECTIONS.MY_SUGGESTIONS ||
            selectedSection === SECTIONS.IN_PROGRESS ||
            selectedSection === SECTIONS.REJECTED
              ? suggestionData.title
              : ""
          }
        />
        {errors?.name?.type === "required" && (
          <Error error={"Это поле обязательно"} />
        )}
        <label className="text-sm mb-2 text-field_label mt-4" for="link">
          Ссылка на курс
        </label>
        <input
          {...register("link", {
            required: true,
          })}
          className={`border-2 border-field_border placeholder-placeholder ${
            selectedSection === SECTIONS.SUGGESTIONS
              ? "bg-field_border"
              : "bg-grey"
          } rounded-lg py-3 px-2 mb-2`}
          type="text"
          id="link"
          name="link"
          value={
            selectedSection === SECTIONS.SUGGESTIONS
              ? suggestionData.link
              : undefined
          }
          defaultValue={
            selectedSection === SECTIONS.MY_SUGGESTIONS ||
            selectedSection === SECTIONS.IN_PROGRESS ||
            selectedSection === SECTIONS.REJECTED
              ? suggestionData.link
              : undefined
          }
        />
        <label className="text-sm mb-2 text-field_label mt-4" for="price">
          Цена курса
        </label>
        <input
          {...register("price", {
            required: true,
          })}
          className={`border-2 border-field_border placeholder-placeholder ${
            selectedSection === SECTIONS.SUGGESTIONS
              ? "bg-field_border"
              : "bg-grey"
          } rounded-lg py-3 px-2 mb-2`}
          type="text"
          id="price"
          name="price"
          value={
            selectedSection === SECTIONS.SUGGESTIONS
              ? suggestionData.price
              : undefined
          }
          defaultValue={
            selectedSection === SECTIONS.MY_SUGGESTIONS ||
            selectedSection === SECTIONS.IN_PROGRESS ||
            selectedSection === SECTIONS.REJECTED
              ? suggestionData.price
              : undefined
          }
        />
        <div className="flex flex-row justify-between items-center">
          <div className="flex flex-col w-[45%]">
            <label
              className="text-sm mb-2 text-field_label mt-4"
              for="start_date"
            >
              Начало курса
            </label>
            <input
              {...register("start_date")}
              className={`border-2 border-field_border placeholder-placeholder ${
                selectedSection === SECTIONS.SUGGESTIONS
                  ? "bg-field_border"
                  : "bg-grey"
              } rounded-lg py-3 px-2 mb-2`}
              type="date"
              id="start_date"
              name="start_date"
              value={
                selectedSection === SECTIONS.SUGGESTIONS ||
                selectedSection === SECTIONS.MY_SUGGESTIONS ||
                selectedSection === SECTIONS.IN_PROGRESS ||
                selectedSection === SECTIONS.REJECTED
                  ? suggestionData.start_date
                  : undefined
              }
              // defaultValue={
              //   selectedSection === SECTIONS.SUGGESTIONS
              //     ? suggestionData.start_date
              //     : undefined
              // }
            />
          </div>
          <div className="flex flex-col w-[45%]">
            <label
              className="text-sm mb-2 text-field_label mt-4"
              for="course_end_date"
            >
              Конец курса
            </label>
            <input
              {...register("end_date")}
              className={`border-2 border-field_border placeholder-placeholder ${
                selectedSection === SECTIONS.SUGGESTIONS
                  ? "bg-field_border"
                  : "bg-grey"
              } rounded-lg py-3 px-2 mb-2`}
              type="date"
              id="end_date"
              name="end_date"
              value={
                selectedSection === SECTIONS.SUGGESTIONS ||
                selectedSection === SECTIONS.MY_SUGGESTIONS ||
                selectedSection === SECTIONS.IN_PROGRESS ||
                selectedSection === SECTIONS.REJECTED
                  ? suggestionData.end_date
                  : undefined
              }
              // defaultValue={
              //   selectedSection === SECTIONS.MY_SUGGESTIONS ||
              //   selectedSection === SECTIONS.IN_PROGRESS
              //     ? suggestionData.end_date
              //     : undefined
              // }
            />
          </div>
        </div>
        <label
          className="text-sm mb-2 text-field_label mt-4"
          for="quarter_of_training"
        >
          Квартал года
        </label>
        <select
          {...register("quarter_of_training", {
            required: true,
          })}
          className={`border-2 border-field_border placeholder-placeholder ${
            selectedSection === SECTIONS.SUGGESTIONS
              ? "bg-field_border"
              : "bg-grey"
          } rounded-lg py-3 px-2 mb-2`}
          type="text"
          id="quarter_of_training"
          name="quarter_of_training"
          value={
            selectedSection === SECTIONS.SUGGESTIONS
              ? suggestionData.quarter_of_training
              : undefined
          }
          defaultValue={
            selectedSection === SECTIONS.MY_SUGGESTIONS ||
            selectedSection === SECTIONS.IN_PROGRESS ||
            selectedSection === SECTIONS.REJECTED
              ? suggestionData.quarter_of_training
              : undefined
          }
        >
          <option value="Q1">Q1</option>
          <option value="Q2">Q2</option>
          <option value="Q3">Q3</option>
          <option value="Q4">Q4</option>
        </select>
        {selectedSection !== SECTIONS.MY_SUGGESTIONS &&
          selectedSection !== SECTIONS.ADD_SUGGESTION && (
            <>
              <label
                className="text-sm mb-2 text-field_label mt-4"
                for="reason"
              >
                Мотивационное письмо
              </label>
              <textarea
                {...register("reason", {
                  required: true,
                })}
                defaultValue={
                  selectedSection === SECTIONS.IN_PROGRESS ||
                  selectedSection === SECTIONS.REJECTED
                    ? suggestionData.reason
                    : undefined
                }
                className={`border-2 border-field_border placeholder-placeholder rounded-lg py-3 px-2 mb-2`}
                name="reason"
                id="reason"
              ></textarea>
            </>
          )}

        <button
          className="flex self-end justify-center w-fix bg-red px-4 py-3 my-2 rounded-lg mt-8"
          type="submit"
        >
          <p className="text-white">Оставить заявку</p>
        </button>
      </form>
    </div>
  );
};
