import { useDispatch } from "react-redux";
import { useState } from "react";
import CollapseArrow from "./../assets/CollapseArrow.png";
import axios from "axios";
import { BACKEND_URL, SECTIONS } from "./Constants";
import { updateSwitchCounter } from "../store/slices/UpdateSlice";

export const Application = (props) => {

  const [detailed, setDetailed] = useState(false);

  const dispatch = useDispatch();

  const closeDetailed = () => {
    if (setDetailed) setDetailed(false);
  };

  const options = {
    year: "numeric",
    month: "short",
    day: "numeric",
  };

  const changeStatus = (_status) => {
    console.log(_status);
    const token = localStorage.getItem("token");
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    axios
      .patch(
        BACKEND_URL +
          `managers/update_request_status/${props.id}?status=${_status}`
      )
      .then((response) => {
        console.log(response);
        dispatch(updateSwitchCounter());
      });
  };

  return (
    <div className="flex flex-row justify-between w-full h-fit bg-white rounded-xl mt-6 py-6 px-5">
      <div className="flex flex-col justify-between">
        <h2 className="text-2xl font-bold">{props.full_name}</h2>
        <p className="text-lg mt-4 font-textRegular">{props.title}</p>
        {detailed && (
          <>
            <div className="flex flex-row">
              <p className="mt-3 font-textRegular">
                {props.quarter_of_training} •{" "}
                {new Date(props.start_date).toLocaleDateString(
                  "ru-RU",
                  options
                )}{" "}
                -{" "}
                {new Date(props.end_date).toLocaleDateString("ru-RU", options)}
              </p>
            </div>
            <div className="flex flex-row mt-3">
              <p className="font-textRegular">Ссылка •</p>
              <a
                href={props.link}
                className="underline ml-2 text-link font-textRegular"
              >
                {props.link}
              </a>
            </div>
            <p className="text-field_label font-textMedium mt-5">
              {props.status}
            </p>
          </>
        )}
      </div>

      <div
        className={`flex flex-col items-end ${
          detailed ? "justify-between" : "justify-end"
        }`}
      >
        {detailed ? (
          <>
            <div className="flex justify-center items-center flex-row">
              <p className="block font-semibold text-lg font-textRegular">
                {props.price} руб
              </p>
              <img
                onClick={() => {
                  closeDetailed();
                }}
                className="h-14 w-14"
                src={CollapseArrow}
                alt="Collapse Arrow"
              />
            </div>
            {props.status === SECTIONS.IN_PROGRESS && (
              <div className="flex flex-row">
                <button
                  onClick={() => changeStatus(SECTIONS.APPROVED)}
                  className="bg-red p-2 my-2 mr-3 rounded px-5 text-white"
                >
                  Одобрить
                </button>
                <button
                  onClick={() => changeStatus(SECTIONS.REJECTED)}
                  className="bg-grey p-2 my-2 rounded px-5 text-black"
                >
                  Отклонить
                </button>
              </div>
            )}
            {props.status === SECTIONS.APPROVED && (
              <div className="flex flex-row">
                <button
                  onClick={() => changeStatus(SECTIONS.IN_PROGRESS)}
                  className="bg-red p-2 my-2 mr-3 rounded px-5 text-white"
                >
                  Рассмотреть
                </button>
                <button
                  onClick={() => changeStatus(SECTIONS.REJECTED)}
                  className="bg-grey p-2 my-2 rounded px-5 text-black"
                >
                  Отклонить
                </button>
              </div>
            )}
            {props.status === SECTIONS.REJECTED && (
              <div className="flex flex-row">
                <button
                  onClick={() => changeStatus(SECTIONS.APPROVED)}
                  className="bg-red p-2 my-2 mr-3 rounded px-5 text-white"
                >
                  Одобрить
                </button>
                <button
                  onClick={() => changeStatus(SECTIONS.IN_PROGRESS)}
                  className="bg-grey p-2 my-2 rounded px-5 text-black"
                >
                  Рассмотреть
                </button>
              </div>
            )}
          </>
        ) : (
          <button
            onClick={() => setDetailed(true)}
            className="bg-red p-2 my-2 rounded px-5 text-white"
          >
            Подробнее
          </button>
        )}
      </div>
    </div>
  );
};
