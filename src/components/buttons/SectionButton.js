import { useDispatch, useSelector } from "react-redux";
import { setSelectedSection } from "../../store/slices/SelectedSectionSlice";
import {
  openSuggestionForm,
  openUserForm,
} from "../../store/slices/ModalSlice";

export const SectionButton = (props) => {
  const selectedSection = useSelector(
    (state) => state.selectedSectionReducer.selectedSection
  );

  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(setSelectedSection(props.section));
    if (props.openSuggestionForm) {
      dispatch(openSuggestionForm());
    }
    if (props.openUserForm) {
      dispatch(openUserForm());
    }
  };

  return (
    <button
      onClick={() => {
        handleClick();
      }}
      className={`px-5 py-3 text-lg rounded-lg mr-4 
    ${selectedSection === props.section ? "bg-button_active" : "bg-white"} `}
    >
      {props.text} {props.number !== undefined && props.number}
    </button>
  );
};
