import { useDispatch} from "react-redux";
import { setSelectedSection } from "../../store/slices/SelectedSectionSlice";
import { SECTIONS } from "../Constants";
import BackArrow from "../../assets/BackArrow.png";

export const BackButton = () => {

  const dispatch = useDispatch();

  return (
    <button
      onClick={() => dispatch(setSelectedSection(SECTIONS.SUGGESTIONS))}
      className={`px-5 py-3 font-semibold text-lg rounded-lg mr-4 bg-white `}
    >
      <img src={BackArrow} alt="Back Arrow"></img>
    </button>
  );
};
