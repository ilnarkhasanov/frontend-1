import { useDispatch } from "react-redux";
import CloseCross from "../assets/CloseCross.png";
import { closeEmailNotification } from "../store/slices/ModalSlice";
import { useNavigate } from "react-router-dom";

export const EmailNotification = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate()
  return (
    <div
      className="absolute top-[25%] left-1/2 transform -translate-x-1/2 -translate-y-[20%] w-1/3 h-fit
    rounded-xl bg-field_border flex flex-col justify-around  px-5 py-6"
    >
      <p className="text-center text-xl font-compactRegular">
        Пароль и логин отправлены на указанную почту
      </p>
      <div className="relative flex justify-end items-end w-full bg-field_border">
        <img
          className="absolute h-8 w-8 bottom-2"
          onClick={() => {
            dispatch(closeEmailNotification());
            navigate("/");
          }}
          src={CloseCross}
          alt="Close Cross"
        ></img>
      </div>
    </div>
  );
};
