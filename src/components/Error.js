export const Error = (props) => {
  return <p className="w-5/6 text-red text-xs py-1">{props.error}</p>;
};
