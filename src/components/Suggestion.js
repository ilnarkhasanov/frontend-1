import { useDispatch, useSelector } from "react-redux";
import { BACKEND_URL, SECTIONS } from "./Constants";
import { openSuggestionForm } from "../store/slices/ModalSlice";
import { setSuggestionData } from "../store/slices/SelectedSuggestionSlice";
import axios from "axios";
import { updateSwitchCounter } from "../store/slices/UpdateSlice";

export const Suggestion = (props) => {
  const selectedSection = useSelector(
    (state) => state.selectedSectionReducer.selectedSection
  );

  const sendRequest = () => {
    const SuggestionData = {
      title: props.title,
      quarter: props.quarter,
      start_date: props.start_date,
      end_date: props.end_date,
      link: props.link,
      price: props.price,
    };
    dispatch(setSuggestionData(SuggestionData));
    dispatch(openSuggestionForm());
  };

  const deleteSuggestion = () => {
    axios.delete(BACKEND_URL + `managers/suggestion/${props.id}`).then(() => {
      dispatch(updateSwitchCounter());
    });
  };

  const updateSuggestion = () => {
    const suggestionData = {
      title: props.title,
      quarter: props.quarter,
      start_date: props.start_date,
      end_date: props.end_date,
      link: props.link,
      price: props.price,
      id: props.id,
    };
    dispatch(setSuggestionData(suggestionData));
    dispatch(openSuggestionForm());
  };

  const deleteRequest = () => {
    axios
      .delete(BACKEND_URL + `requests/delete_request/${props.id}`)
      .then(() => {
        dispatch(updateSwitchCounter());
      });
  };

  const updateRequest = () => {
    const requestData = {
      title: props.title,
      quarter: props.quarter,
      start_date: props.start_date,
      end_date: props.end_date,
      link: props.link,
      price: props.price,
      id: props.id,
      reason: props.reason,
    };
    dispatch(setSuggestionData(requestData));
    dispatch(openSuggestionForm());
  };

  const options = {
    year: "numeric",
    month: "short",
    day: "numeric",
  };

  const dispatch = useDispatch();

  return (
    <div className="flex flex-row justify-between w-full h-fit bg-white rounded-xl mb-6 py-6 px-5">
      <div className="flex flex-col justify-between">
        <h2 className="text-3xl">{props.title}</h2>
        <div className="flex flex-row">
          <p className="mt-3 font-textRegular">
            {props.quarter} •{" "}
            {new Date(props.start_date).toLocaleDateString("ru-RU", options)} -{" "}
            {new Date(props.end_date).toLocaleDateString("ru-RU", options)}
          </p>
        </div>
        <div className="flex flex-row mt-3">
          <p className="font-textRegular">Ссылка •</p>
          <a
            href={props.link}
            className="underline ml-2 text-link font-textRegular"
          >
            {props.link}
          </a>
        </div>
        {selectedSection === SECTIONS.APPROVED && (
          <p className="mt-5 text-field_label font-textMedium">Одобрено</p>
        )}
        {selectedSection === SECTIONS.IN_PROGRESS && (
          <p className="mt-5 text-field_label font-textMedium">
            В статусе ожидания
          </p>
        )}
        {selectedSection === SECTIONS.REJECTED && (
          <p className="mt-5 text-field_label font-textMedium">Отклонено</p>
        )}
      </div>

      <div className="flex flex-col justify-between items-end">
        <p className="block text-xl font-wide">{props.price} руб</p>
        {(selectedSection === SECTIONS.IN_PROGRESS ) && (
          <div className="flex flex-row">
            <button
              className="bg-red p-2 my-2 mr-3 rounded px-5 text-white"
              onClick={() => {
                updateRequest();
              }}
            >
              Изменить
            </button>
            <button
              onClick={() => {
                deleteRequest();
              }}
              className="bg-grey p-2 my-2 rounded px-5 text-black"
            >
              Удалить
            </button>
          </div>
        )}
        {selectedSection === SECTIONS.REJECTED && (
          <div className="flex flex-row">
            <button
              className="bg-red p-2 my-2 mr-3 rounded px-5 text-white"
              onClick={() => {
                updateRequest();
              }}
            >
              Изменить
            </button>

          </div>
        )}
        {selectedSection === SECTIONS.SUGGESTIONS && (
          <button
            onClick={() => sendRequest()}
            className="bg-red p-2 my-2 rounded px-5 text-white"
          >
            Подать заявку
          </button>
        )}
        {selectedSection === SECTIONS.MY_SUGGESTIONS && (
          <div className="flex flex-row">
            <button
              onClick={() => updateSuggestion()}
              className="bg-red p-2 my-2 mr-3 rounded px-5 text-white"
            >
              Изменить
            </button>
            <button
              onClick={() => deleteSuggestion()}
              className="bg-grey p-2 my-2 rounded px-5 text-black"
            >
              Удалить
            </button>
          </div>
        )}
      </div>
    </div>
  );
};
