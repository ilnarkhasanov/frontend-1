export const SECTIONS = {
    MY_APPLICATION: "My Applications",
    BACK: "<-",
    APPROVED: "Approved",
    IN_PROGRESS: "In Progress",
    REJECTED: "Rejected",
    SUGGESTIONS: "Suggestions",
    MY_SUGGESTIONS: "My Suggestions",
    NEW_APPLICATION: "New Application",
    ADD_SUGGESTION: "Add Suggestion",
    STAFF_APPLICATIONS: "Staff Applications",
    USERS_MANAGEMENT: "User Management",
    ADD_USER: "Add User"
  };

export const HTTP_STATUS = {
  OK: 200,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  OTHER: "other",
};

export const ROLES = {
  ADMIN: "Administrator",
  MANAGER: "Manager",
  USER: "User",
  OWNER: "Owner"
};




export const BACKEND_URL = "https://api.yetanothercourse.pro/";
