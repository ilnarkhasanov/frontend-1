import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  updateSwitch: 0,
};

const updateSlice = createSlice({
  name: "updateSlice",
  initialState,
  reducers: {
    updateSwitchCounter: (state) => {
      state.updateSwitch += 1;
    },
  },
});

export const { updateSwitchCounter } = updateSlice.actions;

export default updateSlice.reducer;
