import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  suggestionData: {},
};

const selectedSuggestionSlice = createSlice({
  name: "SelectedCourseSlice",
  initialState,
  reducers: {
    setSuggestionData: (state, action) => {
      state.suggestionData = action.payload;
    },
  },
});

export const { setSuggestionData } = selectedSuggestionSlice.actions;

export default selectedSuggestionSlice.reducer;
