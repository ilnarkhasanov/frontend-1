import { createSlice } from "@reduxjs/toolkit";
import { SECTIONS } from "../../components/Constants";

const initialState = {
  appoved: 0,
  inProgress: 0,
  rejected: 0,
  userRequests: [],
  managerRequests: [],
  suggestions: [],
  users: [],
};

const dataSlice = createSlice({
  name: "dataSlice",
  initialState,
  reducers: {
    setUserRequests: (state, action) => {
      state.userRequests = action.payload;
      state.appoved = state.userRequests.filter(
        (request) => request.status === SECTIONS.APPROVED
      ).length;
      state.inProgress = state.userRequests.filter(
        (request) => request.status === SECTIONS.IN_PROGRESS
      ).length;
      state.rejected = state.userRequests.filter(
        (request) => request.status === SECTIONS.REJECTED
      ).length;
    },
    setManagerRequests: (state, action) => {
      state.managerRequests = action.payload;
    },
    setSuggestions: (state, action) => {
      state.suggestions = action.payload;
      },
    setUsers: (state, action) => {
      state.users = action.payload;
    },
  },
});

export const { setUserRequests, setManagerRequests, setSuggestions, setUsers } =
  dataSlice.actions;

export default dataSlice.reducer;
