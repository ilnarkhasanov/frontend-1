import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  selectedUser: {},
};

const selectedSectionSlice = createSlice({
  name: "SelectedSectionSlice",
  initialState,
  reducers: {
    setSelectedUser: (state, action) => {
      state.selectedUser = action.payload;
    },
  },
});

export const { setSelectedUser} = selectedSectionSlice.actions;

export default selectedSectionSlice.reducer;
