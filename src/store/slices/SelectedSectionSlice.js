import { createSlice } from "@reduxjs/toolkit";
import { SECTIONS } from "../../components/Constants";

const initialState = {
  selectedSection: SECTIONS.SUGGESTIONS,
};

const selectedSectionSlice = createSlice({
  name: "SelectedSectionSlice",
  initialState,
  reducers: {
    setSelectedSection: (state, action) => {
      state.selectedSection = action.payload;
    },
  },
});

export const { setSelectedSection } = selectedSectionSlice.actions;

export default selectedSectionSlice.reducer;
