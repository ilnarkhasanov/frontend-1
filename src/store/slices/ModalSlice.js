import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isSuggestionFormOpen: false,
  isUserFormOpen: false,
  isEmailNotificationOpen: false
};

const modalSlice = createSlice({
  name: "modalSlice",
  initialState,
  reducers: {
    closeSuggestionForm: (state) => {
      state.isSuggestionFormOpen = false;
    },
    openSuggestionForm: (state) => {
      state.isSuggestionFormOpen = true;
    },
    closeUserForm: (state) => {
      state.isUserFormOpen = false;
    },
    openUserForm: (state) => {
      state.isUserFormOpen = true;
    },
    openEmailNotification: (state) => {
      state.isEmailNotificationOpen = true;
    },
    closeEmailNotification: (state) => {
      state.isEmailNotificationOpen = false;
    }
  },
});

export const {
  closeSuggestionForm,
  openSuggestionForm,
  closeUserForm,
  openUserForm,
  openEmailNotification,
  closeEmailNotification
} = modalSlice.actions;

export default modalSlice.reducer;
