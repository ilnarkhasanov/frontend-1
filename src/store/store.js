import { combineReducers, configureStore } from "@reduxjs/toolkit";
import selectedSectionReducer from "./slices/SelectedSectionSlice";
import modalReducer from "./slices/ModalSlice"
import selectedSuggestionReducer from "./slices/SelectedSuggestionSlice";
import dataReducer from "./slices/DataSlice";
import updateReducer from './slices/UpdateSlice'
import selectedUserReducer from "./slices/SelectedUserSlice";

const rootReducer = combineReducers({
    selectedSectionReducer,
    modalReducer,
    selectedSuggestionReducer,
    dataReducer,
    updateReducer,
    selectedUserReducer
});

const store = configureStore({
    reducer: rootReducer,
});
    
export default store;