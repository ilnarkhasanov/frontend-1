/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      red: "#FF0032",
      grey: "#F2F3F7",
      white: "#FFFFFF",
      field_border: "#D7DBE3",
      placeholder: "#828585",
      field_label: "#69737D",
      button_active: "#E4E7ED",
      link: "#0A58A6",
    },
    fontFamily: {
      ultraExtendedBold: ["UltraExtendedBold"],
      compactRegular: ["CompactRegular"],
      compactMedium: ["CompactMedium"],
      textRegular: ["TextRegular"],
      textMedium: ["TextMedium"],
      wide: ["Wide"],
    },
  },
  plugins: [],
};
